package com.example.course.exceptions;


public class AlreadyExistException extends Exception {
    public AlreadyExistException(String message) { super(message); }
}
