package com.example.course.connections;

import lombok.SneakyThrows;
import org.apache.commons.dbcp.BasicDataSource;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DBCPDataSource {
    private static BasicDataSource ds = new BasicDataSource();

    private static Properties properties = new Properties();
    private static final String JDBC_PROPERTY_FILE = "C:\\Users\\tasvl\\IdeaProjects\\CinemaGradle\\src\\main\\resources\\jdbc.properties";

    static {
        FileReader reader = setReader();
        try {
            properties.load(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ds.setUrl(properties.getProperty("db.conn.url"));
        ds.setUsername(properties.getProperty("db.username"));
        ds.setPassword(properties.getProperty("db.password"));
        ds.setMinIdle(5);
        ds.setMaxIdle(10);
        ds.setMaxOpenPreparedStatements(100);
    }

    public static Connection getConnection() throws SQLException {
        Connection connection = ds.getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

    @SneakyThrows
    private static FileReader setReader() {
        return new FileReader(JDBC_PROPERTY_FILE);
    }

    public DBCPDataSource(){ }
}
