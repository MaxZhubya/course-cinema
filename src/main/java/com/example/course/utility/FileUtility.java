package com.example.course.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
@UtilityClass
public class FileUtility implements Serializable {

    public static <T> Map<UUID, T> deserializeData(File file, Class<T> t) {
        ObjectMapper objectMapper = JsonMapper.builder()
                .findAndAddModules().build();

        try (FileInputStream fin = new FileInputStream(file)) {
            return objectMapper.readValue(fin, TypeFactory.defaultInstance().constructMapType(HashMap.class, UUID.class, t));
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return new HashMap<>();
    }

    public static <T> void serializeData(File file, Map<UUID, T> objectMap) {
        ObjectMapper objectMapper = JsonMapper.builder()
                .findAndAddModules().build();

        try (FileOutputStream fin = new FileOutputStream(file)) {
            objectMapper.writeValue(fin, objectMap);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public static File getUserFile() {
        return new File("E://Max_Progs//datafiles//Users.json");
    }

    public static File getMovieFile() {
        return new File("E://Max_Progs//datafiles//Movies.json");
    }

    public static File getTicketFile() {
        return new File("E://Max_Progs//datafiles//Tickets.json");
    }

    @SneakyThrows
    public static File getTempFile() {
        return File.createTempFile("TempFile", ".json");
    }
}
