package com.example.course.utility;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Slf4j
@UtilityClass
public class DBUtility {

    @SneakyThrows
    public static Connection initConnectionWithTables(Connection connection) {
        log.info("Entering method");
        String userTable = "CREATE MEMORY TABLE users\n" +
                "(\n" +
                "    id uuid NOT NULL DEFAULT random_uuid(),\n" +
                "    name varchar(30) NOT NULL,\n" +
                "    phone varchar(20) unique NOT NULL,\n" +
                "    PRIMARY KEY (id)\n" +
                ")";
        String movieTable = "CREATE MEMORY TABLE movies\n" +
                "(\n" +
                "    id uuid NOT NULL DEFAULT random_uuid(),\n" +
                "    title varchar(30) unique NOT NULL,\n" +
                "    duration integer NOT NULL,\n" +
                "    genres array,\n" +
                "    rating double NOT NULL,\n" +
                "    PRIMARY KEY (id)\n" +
                ")";
        String ticketTable = "CREATE MEMORY TABLE tickets\n" +
                "(\n" +
                "    id uuid NOT NULL DEFAULT random_uuid(),\n" +
                "    movie_id uuid NOT NULL,\n" +
                "    user_id uuid NOT NULL,\n" +
                "    price double NOT NULL,\n" +
                "    start timestamp NOT NULL,\n" +
                "    PRIMARY KEY (id),\n" +
                "    FOREIGN KEY (movie_id)\n" +
                "        REFERENCES movies(id),\n" +
                "    FOREIGN KEY (user_id)\n" +
                "        REFERENCES users(id)\n" +
                ")";

        try (PreparedStatement createUsersStatement = connection.prepareStatement(userTable);
             PreparedStatement createMoviesStatement = connection.prepareStatement(movieTable);
             PreparedStatement createTicketsStatement = connection.prepareStatement(ticketTable)) {
            createUsersStatement.execute();
            createMoviesStatement.execute();
            createTicketsStatement.execute();
            log.info("Method result : tables USERS:MOVIES:TICKETS were created");
        } catch (SQLException e) {
            log.error("Caught SQLException : {}", e.getMessage());
        }
        return connection;
    }
}
