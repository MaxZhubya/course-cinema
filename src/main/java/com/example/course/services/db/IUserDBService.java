package com.example.course.services.db;

import com.example.course.entities.User;
import com.example.course.interfaces.generics.GenericService;

public interface IUserDBService extends GenericService<User> {
}
