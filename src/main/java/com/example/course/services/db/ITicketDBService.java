package com.example.course.services.db;

import com.example.course.entities.Movie;
import com.example.course.entities.Ticket;
import com.example.course.entities.User;
import com.example.course.interfaces.generics.GenericService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface ITicketDBService extends GenericService<Ticket> {

    void deleteByUserId(UUID userId);
    void deleteByMovieId(UUID userId);

    List<Movie> getMoviesForToday();
    List<User> getUsersForParticularSession(Movie movie, LocalDateTime sessionTime);
    float getSumForMonth(int year, int month);
    List<Movie> getMovieRateForVisitingCount();
    List<Movie> getMoviesWithMinThicketsSold(int number);
}
