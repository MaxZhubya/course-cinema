package com.example.course.services.db.impls;

import com.example.course.entities.Movie;
import com.example.course.repositories.db.MovieDBRepository;
import com.example.course.services.db.IMovieDBService;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
public class MovieDBServiceImpl implements IMovieDBService {

    private MovieDBRepository repository;
    private TicketDBServiceImpl ticketDBService;

    public MovieDBServiceImpl(Connection connection) {
        this.repository = new MovieDBRepository(connection);
        this.ticketDBService = new TicketDBServiceImpl(connection);
    }

    @Override
    public List<Movie> getAll() {
        log.info("Entering method");
        return repository.findAll();
    }

    @Override
    public Optional<Movie> getById(UUID id) {
        log.info("Entering method");
        return repository.findById(id);
    }

    @Override
    public void create(Movie movie) {
        log.info("Entering method");
        repository.insert(movie);
    }

    @Override
    public void update(Movie movie) {
        log.info("Entering method");
        repository.update(movie);
    }

    @Override
    public void delete(UUID id) {
        log.info("Entering method");
        ticketDBService.deleteByMovieId(id);
        repository.delete(id);
    }
}
