package com.example.course.services.db;

import com.example.course.entities.Movie;
import com.example.course.interfaces.generics.GenericService;

public interface IMovieDBService extends GenericService<Movie> {
}
