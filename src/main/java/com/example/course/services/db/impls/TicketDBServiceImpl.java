package com.example.course.services.db.impls;

import com.example.course.entities.Movie;
import com.example.course.entities.Ticket;
import com.example.course.entities.User;
import com.example.course.repositories.db.TicketDBRepository;
import com.example.course.services.db.ITicketDBService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
public class TicketDBServiceImpl implements ITicketDBService {

    private TicketDBRepository repository;

    public TicketDBServiceImpl(Connection connection) {
        this.repository = new TicketDBRepository(connection);
    }

    @Override
    public List<Ticket> getAll() {
        log.info("Entering method");
        return repository.findAll();
    }

    @Override
    public Optional<Ticket> getById(UUID id) {
        log.info("Entering method");
        return repository.findById(id);
    }

    @Override
    public void create(Ticket ticket) {
        log.info("Entering method");
        repository.insert(ticket);
    }

    @Override
    public void update(Ticket ticket) {
        log.info("Entering method");
        repository.update(ticket);
    }

    @Override
    public void delete(UUID id) {
        log.info("Entering method");
        repository.delete(id);
    }



    @SneakyThrows
    public void deleteByUserId(UUID userId) {
        log.info("Entering business method");
        repository.deleteByUserId(userId);
    }

    @SneakyThrows
    public void deleteByMovieId(UUID movieId) {
        log.info("Entering business method");
        repository.deleteByMovieId(movieId);
    }


    public List<Movie> getMoviesForToday() {
        log.info("Entering business method");
        return repository.getMoviesForToday();
    }

    public List<User> getUsersForParticularSession(Movie movie, LocalDateTime sessionTime) {
        log.info("Entering business method");
        return repository.getUsersForParticularSession(movie, sessionTime);
    }

    public float getSumForMonth(int year, int month) {
        log.info("Entering business method");
        return repository.getSumForMonth(year, month);
    }

    public List<Movie> getMovieRateForVisitingCount() {
        log.info("Entering business method");
        return repository.getMovieRateForVisitingCount();
    }

    public List<Movie> getMoviesWithMinThicketsSold(int number) {
        log.info("Entering business method");
        return repository.getMoviesWithMinThicketsSold(number);
    }
}
