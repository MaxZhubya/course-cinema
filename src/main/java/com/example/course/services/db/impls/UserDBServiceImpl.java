package com.example.course.services.db.impls;

import com.example.course.entities.User;
import com.example.course.repositories.db.UserDBRepository;
import com.example.course.services.db.IUserDBService;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
public class UserDBServiceImpl implements IUserDBService {

    private UserDBRepository repository;
    private TicketDBServiceImpl ticketDBService;

    public UserDBServiceImpl(Connection connection) {
        this.repository = new UserDBRepository(connection);
        this.ticketDBService = new TicketDBServiceImpl(connection);
    }

    @Override
    public List<User> getAll() {
        log.info("Entering method");
        return repository.findAll();
    }

    @Override
    public Optional<User> getById(UUID id) {
        log.info("Entering method");
        return repository.findById(id);
    }

    @Override
    public void create(User user) {
        log.info("Entering method");
        repository.insert(user);
    }

    @Override
    public void update(User user) {
        log.info("Entering method");
        repository.update(user);
    }

    @Override
    public void delete(UUID id) {
        log.info("Entering method");
        ticketDBService.deleteByUserId(id);
        repository.delete(id);
    }
}
