package com.example.course.services.json.impls;

import com.example.course.entities.Movie;
import com.example.course.entities.Ticket;
import com.example.course.entities.User;
import com.example.course.repositories.json.TicketJsonRepository;
import com.example.course.services.json.ITicketJsonService;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class TicketJsonServiceImpl implements ITicketJsonService {

    private TicketJsonRepository ticketJsonRepository;

    public TicketJsonServiceImpl(File file) {
        this.ticketJsonRepository = new TicketJsonRepository(file);
    }

    @Override
    public List<Ticket> getAll() {
        log.info("Entering method");
        return ticketJsonRepository.getAll();
    }

    @Override
    public Optional<Ticket> getById(UUID id) {
        log.info("Entering method");
        return ticketJsonRepository.getById(id);
    }

    @Override
    public void create(Ticket ticket) {
        log.info("Entering method");
        ticketJsonRepository.create(ticket);
    }

    @Override
    public void update(Ticket ticket) {
        log.info("Entering method");
        ticketJsonRepository.update(ticket);
    }

    @Override
    public void delete(UUID id) {
        log.info("Entering method");
        ticketJsonRepository.delete(id);
    }


    public List<Movie> getMoviesForToday() {
        log.info("Entering business method");
        List<Movie> movieList = new ArrayList<>();

        ticketJsonRepository.getAll().stream()
                .filter(ticket -> LocalDate.now().compareTo(ChronoLocalDate.from(ticket.getStart())) == 0)
                .forEach(ticket -> {
                    if (!movieList.contains(ticket.getMovie()))
                        movieList.add(ticket.getMovie());
                });
        log.info("Method result : {}", movieList);
        return movieList;
    }


    public List<User> getUsersForParticularSession(Movie movie, LocalDateTime sessionTime) {
        log.info("Entering business method");
        List<User> userList = new ArrayList<>();

        ticketJsonRepository.getAll().stream()
                .filter(ticket -> movie.equals(ticket.getMovie()) && sessionTime.equals(ticket.getStart()))
                .forEach(ticket -> userList.add(ticket.getUser()));
        log.info("Method result : {}", userList);
        return userList;
    }


    public DoubleSummaryStatistics getSumForMonth(int year, int month) {
        log.info("Entering business method");
        return ticketJsonRepository.getAll().stream()
                .filter(ticket -> ticket.getStart().getYear() == year && ticket.getStart().getMonthValue() == month)
                .collect(Collectors.summarizingDouble(Ticket::getPrice));
    }


    public List<Movie> getMovieRateForVisitingCount() {
        log.info("Entering business method");
        return ticketJsonRepository.getAll().stream()
                .collect(Collectors.groupingBy(Ticket::getMovie))
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.comparingInt(List::size)))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }


    public List<Movie> getMoviesWithMinThicketsSold(int number) {
        log.info("Entering business method");
        return ticketJsonRepository.getAll().stream()
                .collect(Collectors.groupingBy(Ticket::getMovie))
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue().size() < number)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}
