package com.example.course.services.json.impls;

import com.example.course.entities.Movie;
import com.example.course.repositories.json.MovieJsonRepository;
import com.example.course.services.json.IMovieJsonService;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
public class MovieJsonServiceImpl implements IMovieJsonService {

    private MovieJsonRepository movieJsonRepository;

    public MovieJsonServiceImpl(File file) {
        this.movieJsonRepository = new MovieJsonRepository(file);
    }

    @Override
    public List<Movie> getAll() {
        log.info("Entering method");
        return movieJsonRepository.getAll();
    }

    @Override
    public Optional<Movie> getById(UUID id) {
        log.info("Entering method");
        return movieJsonRepository.getById(id);
    }

    @Override
    public void create(Movie movie) {
        log.info("Entering method");
        movieJsonRepository.create(movie);
    }

    @Override
    public void update(Movie movie) {
        log.info("Entering method");
        movieJsonRepository.update(movie);
    }

    @Override
    public void delete(UUID id) {
        log.info("Entering method");
        movieJsonRepository.delete(id);
    }
}
