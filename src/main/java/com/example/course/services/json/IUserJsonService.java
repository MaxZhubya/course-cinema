package com.example.course.services.json;

import com.example.course.entities.User;
import com.example.course.exceptions.AlreadyExistException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public interface IUserJsonService {

    List<User> getAll();
    Optional<User> getById(UUID id);
    void create(User user) throws AlreadyExistException;
    void update(User user);
    void delete(UUID id);
}
