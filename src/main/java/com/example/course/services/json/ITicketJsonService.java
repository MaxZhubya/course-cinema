package com.example.course.services.json;

import com.example.course.entities.Movie;
import com.example.course.entities.Ticket;
import com.example.course.entities.User;
import com.example.course.exceptions.AlreadyExistException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public interface ITicketJsonService {

    List<Ticket> getAll();
    Optional<Ticket> getById(UUID id);
    void create(Ticket ticket) throws AlreadyExistException;
    void update(Ticket ticket) throws AlreadyExistException;
    void delete(UUID id);

    List<Movie> getMoviesForToday();
    List<User> getUsersForParticularSession(Movie movie, LocalDateTime sessionTime);
    DoubleSummaryStatistics getSumForMonth(int year, int month);
    List<Movie> getMovieRateForVisitingCount();
    List<Movie> getMoviesWithMinThicketsSold(int number);
}
