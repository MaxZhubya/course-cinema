package com.example.course.services.json;

import com.example.course.entities.Movie;
import com.example.course.exceptions.AlreadyExistException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public interface IMovieJsonService {

    List<Movie> getAll();
    Optional<Movie> getById(UUID id);
    void create(Movie movie) throws AlreadyExistException;
    void update(Movie movie);
    void delete(UUID id);
}
