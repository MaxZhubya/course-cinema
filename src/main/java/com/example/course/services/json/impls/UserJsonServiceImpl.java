package com.example.course.services.json.impls;

import com.example.course.entities.User;
import com.example.course.repositories.json.UserJsonRepository;
import com.example.course.services.json.IUserJsonService;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
public class UserJsonServiceImpl implements IUserJsonService {

    private UserJsonRepository userJsonRepository;

    public UserJsonServiceImpl(File file) {
        this.userJsonRepository = new UserJsonRepository(file);
    }

    @Override
    public List<User> getAll() {
        log.info("Entering method");
        return userJsonRepository.getAll();
    }

    @Override
    public Optional<User> getById(UUID id) {
        log.info("Entering method");
        return userJsonRepository.getById(id);
    }

    @Override
    public void create(User user) {
        log.info("Entering method");
        userJsonRepository.create(user);
    }

    @Override
    public void update(User user) {
        log.info("Entering method");
        userJsonRepository.update(user);
    }

    @Override
    public void delete(UUID id) {
        log.info("Entering method");
        userJsonRepository.delete(id);
    }
}
