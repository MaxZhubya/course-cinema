package com.example.course.entities;

import lombok.*;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder(toBuilder = true)
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Ticket {

    private UUID id;

    @NotNull(message = "Movie can't be null!")
    private Movie movie;

    @NotNull(message = "User can't be null!")
    private User user;

    @NotNull(message = "Price can't be null!")
    private Float price;

    @NotNull(message = "Start can't be null!")
    @FutureOrPresent(message = "Start must be in Present or Future!")
    private LocalDateTime start;

}
