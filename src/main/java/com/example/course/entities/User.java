package com.example.course.entities;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.UUID;

@Data
@Builder(toBuilder = true)
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private UUID id;

    @NotEmpty(message = "Name can't be empty!")
    private String name;

    @Pattern(regexp = "^((\\+38\\s)((\\(\\d{3}\\))\\s(\\d{2})\\s(\\d{2})\\s(\\d{3})))$", message = "Phone must be as: +38 (xxx) xx xx xxx.")
    private String phone;

}
