package com.example.course.entities;

import com.example.course.entities.enums.GenreTypes;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.validation.constraints.*;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Data
@Builder(toBuilder = true)
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Movie {

    private UUID id;

    @NotEmpty(message = "Title can't be empty!")
    private String title;

    @NotNull(message = "Duration can't be null!")
    private Integer duration;

    private List<GenreTypes> genres;

    @Min(value = 0, message = "Rating can't be < 0!")
    @Max(value = 10, message = "Rating can't be > 10!")
    private Float rating;

    @JsonIgnore
    public String[] getStringsGenres() {
        return Arrays.stream(genres.toArray())
                .map(Object::toString)
                .toArray(String[]::new);

    }
}
