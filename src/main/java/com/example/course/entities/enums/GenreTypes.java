package com.example.course.entities.enums;

public enum GenreTypes {

    DRAMA ("Drama genre"),
    ACTION ("Action genre"),
    FANTASY ("Fantasy genre"),
    COMEDY ("Comedy genre"),
    HORROR ("Horror genre");

    private String description;

    GenreTypes() {}

    GenreTypes(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
