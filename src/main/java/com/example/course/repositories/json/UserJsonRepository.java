package com.example.course.repositories.json;

import com.example.course.entities.User;
import com.example.course.exceptions.BadRequestException;
import com.example.course.utility.FileUtility;
import lombok.extern.slf4j.Slf4j;

import javax.validation.*;
import java.io.File;
import java.util.*;

@Slf4j
public class UserJsonRepository {

    private File file;
    private Map<UUID, User> userMap;

    private Validator validator;
    private Set<ConstraintViolation<User>> violations = new HashSet<>();

    public UserJsonRepository(File file) {
        userMap = FileUtility.deserializeData(file, User.class);
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        this.file = file;
    }

    public List<User> getAll() {
        log.info("Entering method");
        return new ArrayList<>(userMap.values());
    }

    public Optional<User> getById(UUID id) {
        log.info("Entering method");
        return Optional.ofNullable(userMap.get(id));
    }

    public void create(User user) {
        log.info("Entering method");
        validationCheck(user);
        if (ifExist(user.getId())) {
            log.warn("Object already exist : {}", user);
        } else {
            userMap.put(user.getId(), user);
            FileUtility.serializeData(file, userMap);
            log.info("Method result : new User was created");
        }
    }

    public void update(User user) throws BadRequestException {
        log.info("Entering method");
        validationCheck(user);
        if (ifExist(user.getId())) {
            userMap.replace(user.getId(), user);
            FileUtility.serializeData(file, userMap);
            log.info("Method result : User with ID {} was updated", user.getId().toString());
        } else {
            log.error("BadRequestException occured!");
            throw new BadRequestException("Object doesn't exist.");
        }
    }

    public void delete(UUID id) {
        log.info("Entering method");
        if (ifExist(id)) {
            userMap.remove(id);
            FileUtility.serializeData(file, userMap);
            log.info("Method result : User with ID {} was deleted", id.toString());
        } else {
            log.error("BadRequestException occured!");
            throw new BadRequestException("You trying to delete non-existent object.");
        }
    }

    private boolean ifExist(UUID key) {
        log.info("Entering method");
        return userMap.containsKey(key);
    }

    private void validationCheck(User user) {
        log.info("Entering method");
        violations = validator.validate(user);
        if (!violations.isEmpty()) {
            log.error("ValidationException(s) occured!");
            throw new ValidationException("Validation exceptions occured : \n" + violations.toString());
        }
    }
}
