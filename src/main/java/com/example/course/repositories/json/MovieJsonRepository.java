package com.example.course.repositories.json;

import com.example.course.entities.Movie;
import com.example.course.exceptions.BadRequestException;
import com.example.course.utility.FileUtility;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.io.File;
import java.util.*;

@Slf4j
public class MovieJsonRepository {

    private File file;
    private Map<UUID, Movie> movieMap;

    private Validator validator;
    private Set<ConstraintViolation<Movie>> violations = new HashSet<>();

    public MovieJsonRepository(File file) {
        movieMap = FileUtility.deserializeData(file, Movie.class);
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        this.file = file;
    }

    public List<Movie> getAll() {
        log.info("Entering method");
        return new ArrayList<>(movieMap.values());
    }

    public Optional<Movie> getById(UUID id) {
        log.info("Entering method");
        return Optional.ofNullable(movieMap.get(id));
    }

    public void create(Movie movie) {
        log.info("Entering method");
        validationCheck(movie);
        if (ifExist(movie.getId())) {
            log.warn("Object already exist : {}", movie);
        } else {
            movieMap.put(movie.getId(), movie);
            FileUtility.serializeData(file, movieMap);
            log.info("Method result : new Movie was created");
        }
    }

    public void update(Movie movie) throws BadRequestException {
        log.info("Entering method");
        validationCheck(movie);
        if (ifExist(movie.getId())) {
            movieMap.replace(movie.getId(), movie);
            FileUtility.serializeData(file, movieMap);
            log.info("Method result : Movie with ID {} was updated", movie.getId().toString());
        } else {
            log.error("BadRequestException occured!");
            throw new BadRequestException("Object doesn't exist.");
        }
    }

    public void delete(UUID id) {
        log.info("Entering method");
        if (ifExist(id)) {
            movieMap.remove(id);
            FileUtility.serializeData(file, movieMap);
            log.info("Method result : Movie with ID {} was deleted", id.toString());
        } else {
            log.error("BadRequestException occured!");
            throw new BadRequestException("You trying to delete non-existent object.");
        }
    }

    private boolean ifExist(UUID key) {
        log.info("Entering method");
        return movieMap.containsKey(key);
    }

    private void validationCheck(Movie movie) {
        log.info("Entering method");
        violations = validator.validate(movie);
        if (!violations.isEmpty()) {
            log.error("ValidationException(s) occured!");
            throw new ValidationException("Validation exceptions occured : \n" + violations.toString());
        }
    }
}
