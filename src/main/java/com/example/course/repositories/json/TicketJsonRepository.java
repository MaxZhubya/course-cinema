package com.example.course.repositories.json;

import com.example.course.entities.Ticket;
import com.example.course.exceptions.BadRequestException;
import com.example.course.utility.FileUtility;
import lombok.extern.slf4j.Slf4j;

import javax.validation.*;
import java.io.File;
import java.util.*;

@Slf4j
public class TicketJsonRepository {

    private File file;
    private Map<UUID, Ticket> ticketMap;

    private Validator validator;
    private Set<ConstraintViolation<Ticket>> violations = new HashSet<>();

    public TicketJsonRepository(File file) {
        ticketMap = FileUtility.deserializeData(file, Ticket.class);
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        this.file = file;
    }

    public List<Ticket> getAll() {
        log.info("Entering method");
        return new ArrayList<>(ticketMap.values());
    }

    public Optional<Ticket> getById(UUID id) {
        log.info("Entering method");
        return Optional.ofNullable(ticketMap.get(id));
    }

    public void create(Ticket ticket) {
        log.info("Entering method");
        validationCheck(ticket);
        if (ifExist(ticket.getId())) {
            log.warn("Object already exist : {}", ticket);
        } else {
            ticketMap.put(ticket.getId(), ticket);
            FileUtility.serializeData(file, ticketMap);
            log.info("Method result : new Ticket was created");
        }
    }

    public void update(Ticket ticket) throws BadRequestException {
        log.info("Entering method");
        validationCheck(ticket);
        if (ifExist(ticket.getId())) {
            ticketMap.replace(ticket.getId(), ticket);
            FileUtility.serializeData(file, ticketMap);
            log.info("Method result : Ticket with ID {} was updated", ticket.getId().toString());
        } else {
            log.error("BadRequestException occured!");
            throw new BadRequestException("Object doesn't exist.");
        }
    }

    public void delete(UUID id) {
        log.info("Entering method");
        if (ifExist(id)) {
            ticketMap.remove(id);
            FileUtility.serializeData(file, ticketMap);
            log.info("Method result : Ticket with ID {} was deleted", id.toString());
        } else {
            log.error("BadRequestException occured!");
            throw new BadRequestException("You trying to delete non-existent object.");
        }
    }

    private boolean ifExist(UUID key) {
        log.info("Entering method");
        return ticketMap.containsKey(key);
    }

    private void validationCheck(Ticket ticket) {
        log.info("Entering method");
        violations = validator.validate(ticket);
        if (!violations.isEmpty()) {
            log.error("ValidationException(s) occured!");
            throw new  ValidationException("Validation exceptions occured : \n" + violations.toString());
        }
    }
}
