package com.example.course.repositories.db;

import com.example.course.entities.Movie;
import com.example.course.entities.enums.GenreTypes;
import com.example.course.exceptions.BadRequestException;
import com.example.course.interfaces.repositories.IMovieDBRepository;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class MovieDBRepository implements IMovieDBRepository {

    private Validator validator;
    private Connection connection;

    private static final String SQL_EXCEPTION_MESSAGE = "Caught SQLException {}";
    private static final String BAD_REQUEST_EXCEPTION_MESSAGE = "BadRequestException occured";
    private static final String VALIDATION_EXCEPTION_MESSAGE = "ValidationException(s) occured";
    private static final String TRANSACTION_ROLLED_BACK_MESSAGE = "Transaction is being rolled back!";

    private static final String FIND_ALL_QUERY = "select * from movies;";
    private static final String FIND_BY_ID_QUERY = "select * from movies where id = ?;";
    private static final String INSERT_QUERY = "insert into movies (title, duration, genres, rating) values(?, ?, ?, ?);";
    private static final String UPDATE_QUERY = "update movies set title = ?, duration = ?, genres = ?, rating = ? where id = ?;";
    private static final String DELETE_QUERY = "delete from movies where id = ?;";

    public MovieDBRepository(Connection connection) {
        this.validator = Validation.buildDefaultValidatorFactory().getValidator();
        this.connection = connection;
    }

    @Override
    public List<Movie> findAll() {
        log.info("Entering method");
        List<Movie> movieList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_QUERY);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                movieList.add(setMovieFromResultSet(resultSet));
            }
            log.info("Method result : \n{}", movieList);
        } catch (SQLException e) {
            log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
        }
        return movieList;
    }

    @Override
    public Optional<Movie> findById(UUID id) {
        log.info("Entering method");
        Movie movie = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_QUERY)) {
            preparedStatement.setObject(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                movie = setMovieFromResultSet(resultSet);
            }
            log.info("Method result : \n{}", movie);
        } catch (SQLException e) {
            log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
        }
        return Optional.ofNullable(movie);
    }

    @SneakyThrows
    @Override
    public void insert(Movie movie) {
        log.info("Entering method");
        validationCheck(movie);
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY)) {
            preparedStatement.setString(1, movie.getTitle());
            preparedStatement.setInt(2, movie.getDuration());
            preparedStatement.setArray(3, connection.createArrayOf("genre_types", movie.getStringsGenres()));
            preparedStatement.setFloat(4, movie.getRating());
            preparedStatement.executeUpdate();
            connection.commit();
            log.info("Method result : New Movie was created");
        } catch (SQLException e) {
            log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
            try {
                log.info(TRANSACTION_ROLLED_BACK_MESSAGE);
                connection.rollback();
            } catch (SQLException excep) {
                log.error(SQL_EXCEPTION_MESSAGE, excep.getMessage());
            }
        }
    }

    @SneakyThrows
    @Override
    public void update(Movie movie) {
        log.info("Entering method");
        validationCheck(movie);
        if (ifExist(movie.getId())) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY)) {
                preparedStatement.setString(1, movie.getTitle());
                preparedStatement.setInt(2, movie.getDuration());
                preparedStatement.setArray(3, connection.createArrayOf("genre_types", movie.getStringsGenres()));
                preparedStatement.setFloat(4, movie.getRating());
                preparedStatement.setObject(5, movie.getId());
                preparedStatement.executeUpdate();
                connection.commit();
                log.info("Method result : Movie with ID {} was modified", movie.getId().toString());
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
                try {
                    log.info(TRANSACTION_ROLLED_BACK_MESSAGE);
                    connection.rollback();
                } catch (SQLException excep) {
                    log.error(SQL_EXCEPTION_MESSAGE, excep.getMessage());
                }
            }
        } else {
            log.error(BAD_REQUEST_EXCEPTION_MESSAGE);
            throw new BadRequestException("Object doesn't exist.");
        }
    }

    @SneakyThrows
    @Override
    public void delete(UUID id) {
        log.info("Entering method");
        if (ifExist(id)) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY)) {
                preparedStatement.setObject(1, id);
                preparedStatement.executeUpdate();
                connection.commit();
                log.info("Method result : Movie with ID {} was deleted", id.toString());
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
                try {
                    log.info(TRANSACTION_ROLLED_BACK_MESSAGE);
                    connection.rollback();
                } catch (SQLException excep) {
                    log.error(SQL_EXCEPTION_MESSAGE, excep.getMessage());
                }
            }
        } else {
            log.error(BAD_REQUEST_EXCEPTION_MESSAGE);
            throw new BadRequestException("You trying to delete non-existent object.");
        }
    }


    private boolean ifExist(UUID id) {
        log.info("Entering intermediate method");
        return findAll().stream().anyMatch(movie -> movie.getId().equals(id));
    }

    private void validationCheck(Movie movie) {
        log.info("Entering intermediate method");
        Set<ConstraintViolation<Movie>> violations = validator.validate(movie);
        if (!violations.isEmpty()) {
            log.error(VALIDATION_EXCEPTION_MESSAGE);
            throw new ValidationException("Validation exception(s) occured : \n" + violations.toString());
        }
    }

    @SneakyThrows
    private Movie setMovieFromResultSet(ResultSet resultSet) {
        log.info("Entering intermediate method");
        List<GenreTypes> genres = Stream.of((String[]) resultSet.getArray("genres").getArray())
                .map(GenreTypes::valueOf).collect(Collectors.toList());

        return Movie.builder()
                .id(UUID.fromString(resultSet.getString("id")))
                .title(resultSet.getString("title"))
                .duration(resultSet.getInt("duration"))
                .genres(genres)
                .rating(resultSet.getFloat("rating"))
                .build();
    }
}
