package com.example.course.repositories.db;

import com.example.course.entities.User;
import com.example.course.exceptions.BadRequestException;
import com.example.course.interfaces.repositories.IUserDBRepository;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.sql.*;
import java.util.*;

@Slf4j
public class UserDBRepository implements IUserDBRepository {

    private Validator validator;
    private Connection connection;

    private static final String SQL_EXCEPTION_MESSAGE = "Caught SQLException {}";
    private static final String BAD_REQUEST_EXCEPTION_MESSAGE = "BadRequestException occured";
    private static final String VALIDATION_EXCEPTION_MESSAGE = "ValidationException(s) occured";
    private static final String TRANSACTION_ROLLED_BACK_MESSAGE = "Transaction is being rolled back!";

    private static final String FIND_ALL_QUERY = "select * from users;";
    private static final String FIND_BY_ID_QUERY = "select * from users where id = ?;";
    private static final String INSERT_QUERY = "insert into users (name, phone) values(?, ?);";
    private static final String UPDATE_QUERY = "update users set name = ?, phone = ? where id = ?;";
    private static final String DELETE_QUERY = "delete from users where id = ?;";

    @SneakyThrows
    public UserDBRepository(Connection connection) {
        this.validator = Validation.buildDefaultValidatorFactory().getValidator();
        this.connection = connection;
    }

    @Override
    public List<User> findAll() {
        log.info("Entering method");
        List<User> userList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_QUERY);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                userList.add(setUserFromResultSet(resultSet));
            }
            log.info("Method result : \n{}", userList);
        } catch (SQLException e) {
            log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
        }
        return userList;
    }

    @Override
    public Optional<User> findById(UUID id) {
        log.info("Entering method");
        User user = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_QUERY)) {
            preparedStatement.setObject(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user = setUserFromResultSet(resultSet);
            }
            log.info("Method result : \n{}", user);
        } catch (SQLException e) {
            log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
        }
        return Optional.ofNullable(user);
    }

    @SneakyThrows
    @Override
    public void insert(User user) {
        log.info("Entering method");
        validationCheck(user);
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY)) {
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getPhone());
            preparedStatement.executeUpdate();
            connection.commit();
            log.info("Method result : New User was created");
        } catch (SQLException e) {
            log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
            try {
                log.info(TRANSACTION_ROLLED_BACK_MESSAGE);
                connection.rollback();
            } catch (SQLException excep) {
                log.error(SQL_EXCEPTION_MESSAGE, excep.getMessage());
            }
        }
    }

    @SneakyThrows
    @Override
    public void update(User user) {
        log.info("Entering method");
        validationCheck(user);
        if (ifExist(user.getId())) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY)) {
                preparedStatement.setString(1, user.getName());
                preparedStatement.setString(2, user.getPhone());
                preparedStatement.setObject(3, user.getId());
                preparedStatement.executeUpdate();
                connection.commit();
                log.info("Method result : User with ID {} was modified", user.getId().toString());
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
                try {
                    log.info(TRANSACTION_ROLLED_BACK_MESSAGE);
                    connection.rollback();
                } catch (SQLException excep) {
                    log.error(SQL_EXCEPTION_MESSAGE, excep.getMessage());
                }
            }
        } else {
            log.error(BAD_REQUEST_EXCEPTION_MESSAGE);
            throw new BadRequestException("Object doesn't exist.");
        }
    }

    @SneakyThrows
    @Override
    public void delete(UUID id) {
        log.info("Entering method");
        if (ifExist(id)) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY)) {
                preparedStatement.setObject(1, id);
                preparedStatement.executeUpdate();
                connection.commit();
                log.info("Method result : User with ID {} was deleted", id.toString());
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
                try {
                    log.info(TRANSACTION_ROLLED_BACK_MESSAGE);
                    connection.rollback();
                } catch (SQLException excep) {
                    log.error(SQL_EXCEPTION_MESSAGE, excep.getMessage());
                }
            }
        } else {
            log.error(BAD_REQUEST_EXCEPTION_MESSAGE);
            throw new BadRequestException("You trying to delete non-existent object.");
        }
    }


    private boolean ifExist(UUID id) {
        log.info("Entering intermediate method");
        return findAll().stream().anyMatch(user -> user.getId().equals(id));
    }

    private void validationCheck(User user) {
        log.info("Entering intermediate method");
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        if (!violations.isEmpty()) {
            log.error(VALIDATION_EXCEPTION_MESSAGE);
            throw new ValidationException("Validation exception(s) occured : \n" + violations.toString());
        }
    }

    @SneakyThrows
    private User setUserFromResultSet(ResultSet resultSet) {
        log.info("Entering intermediate method");
        return User.builder()
                .id(UUID.fromString(resultSet.getString("id")))
                .phone(resultSet.getString("phone"))
                .name(resultSet.getString("name"))
                .build();
    }
}
