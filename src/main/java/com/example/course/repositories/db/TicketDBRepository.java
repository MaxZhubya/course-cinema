package com.example.course.repositories.db;

import com.example.course.entities.Movie;
import com.example.course.entities.Ticket;
import com.example.course.entities.User;
import com.example.course.entities.enums.GenreTypes;
import com.example.course.exceptions.BadRequestException;
import com.example.course.interfaces.repositories.ITicketDBRepository;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.sql.Date;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class TicketDBRepository implements ITicketDBRepository {

    private Validator validator;
    private Connection connection;

    private static final String SQL_EXCEPTION_MESSAGE = "Caught SQLException {}";
    private static final String BAD_REQUEST_EXCEPTION_MESSAGE = "BadRequestException occured";
    private static final String VALIDATION_EXCEPTION_MESSAGE = "ValidationException(s) occured";
    private static final String TRANSACTION_ROLLED_BACK_MESSAGE = "Transaction is being rolled back!";

    private static final String FIND_ALL_QUERY =
            "select t.id as ticket_id, t.price as ticket_price, t.start as ticket_start, " +
            "m.id as movie_id, m.title as movie_title, m.duration as movie_duration, m.genres as movie_genres, m.rating as movie_rating, " +
            "u.id as user_id, u.name as user_name, u.phone as user_phone " +
            "from tickets as t join movies as m on m.id = t.movie_id join users as u on u.id = t.user_id";

    private static final String FIND_BY_ID_QUERY = FIND_ALL_QUERY.concat(" where t.id = ?");
    private static final String INSERT_QUERY = "insert into tickets (movie_id, user_id, price, start) values(?, ?, ?, ?);";
    private static final String UPDATE_QUERY = "update tickets set movie_id = ?, user_id = ?, price = ?, start = ? where id = ?;";
    private static final String DELETE_QUERY = "delete from tickets where id = ?;";

    private static final String DELETE_BY_USER_ID_QUERY = "delete from tickets where user_id = ?;";
    private static final String DELETE_BY_MOVIE_ID_QUERY = "delete from tickets where movie_id = ?;";


    private static final String GET_MOVIES_FOR_TODAY =
            "select distinct m.id as movie_id, m.title as movie_title, m.duration as movie_duration, m.genres as movie_genres, m.rating as movie_rating " +
            "from tickets as t join movies as m on m.id = t.movie_id where t.start::date = ?";

    private static final String GET_USERS_FOR_PARTICULAR_SESSION =
            "select distinct u.id as user_id, u.name as user_name, u.phone as user_phone " +
            "from tickets as t join users as u on u.id = t.user_id where t.movie_id = ? and t.start::date = ?";

    private static final String GET_SUM_FOR_MONTH =
            "select t.price from tickets as t " +
            "where extract(year from t.start::date) = ? and extract(month from t.start::date) = ?";

    private static final String GET_MOVIE_RATE_FOR_VISITING_COUNT =
            "select distinct m.id as movie_id, m.title as movie_title, m.duration as movie_duration, m.genres as movie_genres, m.rating as movie_rating, count(*) as count " +
            "from tickets as t join movies as m on m.id = t.movie_id " +
            "group by m.id, m.title " +
            "order by count desc, m.title";

    private static final String GET_MOVIES_WITH_MIN_TICKETS_SOLD =
            "select distinct m.id as movie_id, m.title as movie_title, m.duration as movie_duration, m.genres as movie_genres, m.rating as movie_rating, count(*) as count " +
            "from tickets as t join movies as m on m.id = t.movie_id " +
            "group by m.id, m.title " +
            "having count(*) < ?" +
            "order by count desc, m.title";



    public TicketDBRepository(Connection connection) {
        this.validator = Validation.buildDefaultValidatorFactory().getValidator();
        this.connection = connection;
    }

    @Override
    public List<Ticket> findAll() {
        log.info("Entering method");
        List<Ticket> ticketList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_QUERY);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                ticketList.add(setTicketFromResultSet(resultSet));
            }
            log.info("Method result : \n{}", ticketList);
        } catch (SQLException e) {
            log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
        }
        return ticketList;
    }

    @Override
    public Optional<Ticket> findById(UUID id) {
        log.info("Entering method");
        Ticket ticket = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_QUERY)) {
            preparedStatement.setObject(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ticket = setTicketFromResultSet(resultSet);
            }
            log.info("Method result : \n{}", ticket);
        } catch (SQLException e) {
            log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
        }
        return Optional.ofNullable(ticket);
    }

    @SneakyThrows
    @Override
    public void insert(Ticket ticket) {
        log.info("Entering method");
        validationCheck(ticket);
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY)) {
            preparedStatement.setObject(1, ticket.getMovie().getId());
            preparedStatement.setObject(2, ticket.getUser().getId());
            preparedStatement.setFloat(3, ticket.getPrice());
            preparedStatement.setTimestamp(4, Timestamp.valueOf(ticket.getStart()));
            preparedStatement.executeUpdate();
            connection.commit();
            log.info("Method result : New Ticket was created");
        } catch (SQLException e) {
            log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
            try {
                log.info(TRANSACTION_ROLLED_BACK_MESSAGE);
                connection.rollback();
            } catch (SQLException excep) {
                log.error(SQL_EXCEPTION_MESSAGE, excep.getMessage());
            }
        }
    }

    @SneakyThrows
    @Override
    public void update(Ticket ticket) {
        log.info("Entering method");
        validationCheck(ticket);
        if (ifExist(ticket.getId())) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY)) {
                preparedStatement.setObject(1, ticket.getMovie().getId());
                preparedStatement.setObject(2, ticket.getUser().getId());
                preparedStatement.setFloat(3, ticket.getPrice());
                preparedStatement.setTimestamp(4, Timestamp.valueOf(ticket.getStart()));
                preparedStatement.setObject(5, ticket.getId());
                preparedStatement.executeUpdate();
                preparedStatement.executeUpdate();
                connection.commit();
                log.info("Method result : Ticket with ID {} was modified", ticket.getId().toString());
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
                try {
                    log.info(TRANSACTION_ROLLED_BACK_MESSAGE);
                    connection.rollback();
                } catch (SQLException excep) {
                    log.error(SQL_EXCEPTION_MESSAGE, excep.getMessage());
                }
            }
        } else {
            log.error(BAD_REQUEST_EXCEPTION_MESSAGE);
            throw new BadRequestException("Object doesn't exist.");
        }
    }

    @SneakyThrows
    @Override
    public void delete(UUID id) {
        log.info("Entering method");
        if (ifExist(id)) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY)) {
                preparedStatement.setObject(1, id);
                preparedStatement.executeUpdate();
                connection.commit();
                log.info("Method result : Ticket with ID {} was deleted", id.toString());
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
                try {
                    log.info(TRANSACTION_ROLLED_BACK_MESSAGE);
                    connection.rollback();
                } catch (SQLException excep) {
                    log.error(SQL_EXCEPTION_MESSAGE, excep.getMessage());
                }
            }
        } else {
            log.error(BAD_REQUEST_EXCEPTION_MESSAGE);
            throw new BadRequestException("You trying to delete non-existent object.");
        }
    }


    @SneakyThrows
    private Ticket setTicketFromResultSet(ResultSet resultSet) {
        log.info("Entering intermediate method");
        return Ticket.builder()
                .id(UUID.fromString(resultSet.getString("ticket_id")))
                .movie(setMovieFromResultSet(resultSet))
                .user(setUserFromResultSet(resultSet))
                .price(resultSet.getFloat("ticket_price"))
                .start(resultSet.getTimestamp("ticket_start").toLocalDateTime())
                .build();
    }

    @SneakyThrows
    private Movie setMovieFromResultSet(ResultSet resultSet) {
        log.info("Entering intermediate method");
        List<GenreTypes> genres = Stream.of((String[])resultSet.getArray("movie_genres").getArray())
                .map(GenreTypes::valueOf).collect(Collectors.toList());

        return Movie.builder()
                .id(UUID.fromString(resultSet.getString("movie_id")))
                .rating(resultSet.getFloat("movie_rating"))
                .genres(genres)
                .title(resultSet.getString("movie_title"))
                .duration(resultSet.getInt("movie_duration"))
                .build();
    }

    @SneakyThrows
    private User setUserFromResultSet(ResultSet resultSet) {
        log.info("Entering intermediate method");
        return User.builder()
                .id(UUID.fromString(resultSet.getString("user_id")))
                .name(resultSet.getString("user_name"))
                .phone(resultSet.getString("user_phone"))
                .build();
    }

    private boolean ifExist(UUID id) {
        log.info("Entering intermediate method");
        return findAll().stream().anyMatch(ticket -> ticket.getId().equals(id));
    }

    private void validationCheck(Ticket ticket) {
        log.info("Entering intermediate method");
        Set<ConstraintViolation<Ticket>> violations = validator.validate(ticket);
        if (!violations.isEmpty()) {
            log.error(VALIDATION_EXCEPTION_MESSAGE);
            throw new ValidationException("Validation exception(s) occured : \n" + violations.toString());
        }
    }

    private boolean ifHasSuchUser(UUID id) {
        log.info("Entering intermediate method");
        return findAll().stream().anyMatch(ticket -> ticket.getUser().getId().equals(id));
    }

    private boolean ifHasSuchMovie(UUID id) {
        log.info("Entering intermediate method");
        return findAll().stream().anyMatch(ticket -> ticket.getMovie().getId().equals(id));
    }


    @SneakyThrows
    public void deleteByUserId(UUID id) {
        log.info("Entering business method");
        if (ifHasSuchUser(id)) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_USER_ID_QUERY)) {
                preparedStatement.setObject(1, id);
                preparedStatement.executeUpdate();
                connection.commit();
                log.info("Method result : Ticket with User_ID {} was deleted", id.toString());
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
                try {
                    log.info(TRANSACTION_ROLLED_BACK_MESSAGE);
                    connection.rollback();
                } catch (SQLException excep) {
                    log.error(SQL_EXCEPTION_MESSAGE, excep.getMessage());
                }
            }
        } else {
            log.error(BAD_REQUEST_EXCEPTION_MESSAGE);
            throw new BadRequestException("You trying to delete non-existent object.");
        }
    }

    @SneakyThrows
    public void deleteByMovieId(UUID id) {
        log.info("Entering business method");
        if (ifHasSuchMovie(id)) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_MOVIE_ID_QUERY)) {
                preparedStatement.setObject(1, id);
                preparedStatement.executeUpdate();
                connection.commit();
                log.info("Method result : Ticket with Movie_ID {} was deleted", id.toString());
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
                try {
                    log.info(TRANSACTION_ROLLED_BACK_MESSAGE);
                    connection.rollback();
                } catch (SQLException excep) {
                    log.error(SQL_EXCEPTION_MESSAGE, excep.getMessage());
                }
            }
        } else {
            log.error(BAD_REQUEST_EXCEPTION_MESSAGE);
            throw new BadRequestException("You trying to delete non-existent object.");
        }
    }


    public List<Movie> getMoviesForToday() {
        log.info("Entering business method");
        List<Movie> movieList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_MOVIES_FOR_TODAY)) {
            preparedStatement.setDate(1, Date.valueOf(LocalDate.now()));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                movieList.add(setMovieFromResultSet(resultSet));
            }
            log.info("Method result : {}", movieList);
        } catch (SQLException e) {
            log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
        }
        return movieList;
    }


    public List<User> getUsersForParticularSession(Movie movie, LocalDateTime sessionTime) {
        log.info("Entering business method");
        List<User> userList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_USERS_FOR_PARTICULAR_SESSION)) {
            preparedStatement.setObject(1, movie.getId());
            preparedStatement.setDate(2, Date.valueOf(sessionTime.toLocalDate()));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                userList.add(setUserFromResultSet(resultSet));
            }
            log.info("Method result : {}", userList);
        } catch (SQLException e) {
            log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
        }
        return userList;
    }


    public float getSumForMonth(int year, int month) {
        log.info("Entering business method");
        float sum = 0;
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_SUM_FOR_MONTH)) {
            preparedStatement.setInt(1, year);
            preparedStatement.setInt(2, month);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                sum += resultSet.getFloat("price");
            }
            log.info("Method result : {}", sum);
        } catch (SQLException e) {
            log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
        }
        return sum;
    }


    public List<Movie> getMovieRateForVisitingCount() {
        log.info("Entering business method");
        List<Movie> movieList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_MOVIE_RATE_FOR_VISITING_COUNT);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                movieList.add(setMovieFromResultSet(resultSet));
            }
            log.info("Method result : {}", movieList);
        } catch (SQLException e) {
            log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
        }
        return movieList;
    }


    public List<Movie> getMoviesWithMinThicketsSold(int number) {
        log.info("Entering business method");
        List<Movie> movieList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_MOVIES_WITH_MIN_TICKETS_SOLD)) {
            preparedStatement.setInt(1, number);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                movieList.add(setMovieFromResultSet(resultSet));
            }
            log.info("Method result : {}", movieList);
        } catch (SQLException e) {
            log.error(SQL_EXCEPTION_MESSAGE, e.getMessage());
        }
        return movieList;
    }
}
