package com.example.course.interfaces.generics;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GenericService<T> {

    List<T> getAll();
    Optional<T> getById(UUID id);
    void create(T object);
    void update(T object);
    void delete(UUID id);
}
