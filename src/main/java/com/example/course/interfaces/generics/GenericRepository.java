package com.example.course.interfaces.generics;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GenericRepository<T> {

    List<T> findAll();
    Optional<T> findById(UUID id);
    void insert(T object);
    void update(T object);
    void delete(UUID id);
}
