package com.example.course.interfaces.repositories;

import com.example.course.entities.User;
import com.example.course.interfaces.generics.GenericRepository;

import java.util.List;

public interface IUserDBRepository extends GenericRepository<User> {

}
