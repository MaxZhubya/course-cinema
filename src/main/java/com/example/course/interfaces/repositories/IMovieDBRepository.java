package com.example.course.interfaces.repositories;

import com.example.course.entities.Movie;
import com.example.course.interfaces.generics.GenericRepository;

public interface IMovieDBRepository extends GenericRepository<Movie> {
}
