package com.example.course.interfaces.repositories;

import com.example.course.entities.Ticket;
import com.example.course.interfaces.generics.GenericRepository;

public interface ITicketDBRepository extends GenericRepository<Ticket> {
}
