package com.example.course.services.json;

import com.example.course.entities.User;
import com.example.course.services.json.impls.UserJsonServiceImpl;
import com.example.course.utility.FileUtility;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.util.List;
import java.util.UUID;

@Slf4j
public class UserJsonServiceTest {

    private UserJsonServiceImpl userService;

    @SneakyThrows
    @BeforeTest
    public void setUp() {
        log.info("Entering BeforeTest");
        File tempFile = FileUtility.getTempFile();
        tempFile.deleteOnExit();
        userService = new UserJsonServiceImpl(tempFile);

        userService.create(User.builder()
                .id(UUID.randomUUID())
                .phone("+38 (999) 99 99 999")
                .name("First Test User")
                .build());
        userService.create(User.builder()
                .id(UUID.randomUUID())
                .phone("+38 (888) 88 88 888")
                .name("Second Test User")
                .build());
        List<User> userList = userService.getAll();
        Assert.assertEquals(userList.size(), 2);
    }

    @Test(groups = { "crud" })
    public void whenGetAll_thenReturnUserList() {
        log.info("Entering Test");
        List<User> foundList = userService.getAll();
        Assert.assertEquals(foundList.size(), 2);
    }


    @Test(groups = { "crud" })
    public void whenGetById_thenReturnSingleUser() {
        log.info("Entering Test");
        List<User> actualList = userService.getAll();
        User expectedUser = actualList.get(0);
        User foundUser = userService.getById(expectedUser.getId()).get();

        Assert.assertNotNull(foundUser);
        Assert.assertSame(foundUser, expectedUser);
    }


    @Test(groups = { "crud" })
    public void whenCreateUser_thenVoid() {
        log.info("Entering Test");
        User user = User.builder()
                .id(UUID.randomUUID())
                .phone("+38 (777) 77 77 777")
                .name("Third Test User")
                .build();
        userService.create(user);

        List<User> existedUsers = userService.getAll();
        Assert.assertTrue(existedUsers.stream().anyMatch(existedUser -> existedUser.getId().equals(user.getId())));
    }


    @Test(groups = { "crud" })
    public void whenUpdateUser_thenVoid() {
        log.info("Entering Test");
        List<User> actualList = userService.getAll();

        User existedUser = actualList.get(0).toBuilder().name("New Test First User").build();
        userService.update(existedUser);

        Assert.assertEquals(userService.getById(existedUser.getId()).get().getName(), "New Test First User");
    }

    @Test(groups = { "crud" })
    public void whenDeleteUser_thenVoid() {
        log.info("Entering Test");
        List<User> actualList = userService.getAll();

        User userToDelete = actualList.get(0);

        userService.delete(userToDelete.getId());
        Assert.assertTrue(userService.getById(userToDelete.getId()).isEmpty());
    }
}
