package com.example.course.services.json;

import com.example.course.entities.Movie;
import com.example.course.entities.enums.GenreTypes;
import com.example.course.services.json.impls.MovieJsonServiceImpl;
import com.example.course.utility.FileUtility;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Slf4j
public class MovieJsonServiceTest {

    private MovieJsonServiceImpl movieService;

    @SneakyThrows
    @BeforeTest
    public void setUp() {
        log.info("Entering BeforeTest");
        File tempFile = FileUtility.getTempFile();
        tempFile.deleteOnExit();
        movieService = new MovieJsonServiceImpl(tempFile);

        movieService.create(Movie.builder()
                .id(UUID.randomUUID())
                .title("First Test Movie Title")
                .duration(123)
                .genres(Arrays.asList(
                        GenreTypes.COMEDY
                ))
                .rating(7.9f)
                .build());
        movieService.create(Movie.builder()
                .id(UUID.randomUUID())
                .title("Second Test Movie Title")
                .duration(197)
                .genres(Arrays.asList(
                        GenreTypes.ACTION,
                        GenreTypes.FANTASY
                ))
                .rating(8.9f)
                .build());
        List<Movie> movieList = movieService.getAll();
        Assert.assertEquals(movieList.size(), 2);
    }

    @Test(groups = { "crud" })
    public void whenGetAll_thenReturnMovieList() {
        log.info("Entering Test");
        List<Movie> foundList = movieService.getAll();
        Assert.assertEquals(foundList.size(), 2);
    }


    @Test(groups = { "crud" })
    public void whenGetById_thenReturnSingleMovie() {
        log.info("Entering Test");
        List<Movie> actualList = movieService.getAll();
        Movie expectedMovie = actualList.get(0);
        Movie foundMovie = movieService.getById(expectedMovie.getId()).get();

        Assert.assertNotNull(foundMovie);
        Assert.assertSame(foundMovie, expectedMovie);
    }


    @Test(groups = { "crud" })
    public void whenCreateMovie_thenVoid() {
        log.info("Entering Test");
        Movie movie = Movie.builder()
                .id(UUID.randomUUID())
                .title("Third Test Movie Title")
                .duration(157)
                .genres(Arrays.asList(
                        GenreTypes.DRAMA
                ))
                .rating(6.2f)
                .build();
        movieService.create(movie);

        List<Movie> existedMovies = movieService.getAll();
        Assert.assertTrue(existedMovies.stream().anyMatch(existedMovie -> existedMovie.getId().equals(movie.getId())));
    }


    @Test(groups = { "crud" })
    public void whenUpdateMovie_thenVoid() {
        log.info("Entering Test");
        List<Movie> actualList = movieService.getAll();

        Movie existedMovie = actualList.get(0).toBuilder().title("New Test First Movie").build();
        movieService.update(existedMovie);

        Assert.assertEquals(movieService.getById(existedMovie.getId()).get().getTitle(), "New Test First Movie");
    }


    @Test(groups = { "crud" })
    public void whenDeleteMovie_thenVoid() {
        log.info("Entering Test");
        List<Movie> actualList = movieService.getAll();

        Movie movieToDelete = actualList.get(0);

        movieService.delete(movieToDelete.getId());
        Assert.assertTrue(movieService.getById(movieToDelete.getId()).isEmpty());
    }
}
