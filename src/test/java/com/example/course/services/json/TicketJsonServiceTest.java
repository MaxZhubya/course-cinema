package com.example.course.services.json;

import com.example.course.entities.Movie;
import com.example.course.entities.Ticket;
import com.example.course.entities.User;
import com.example.course.entities.enums.GenreTypes;
import com.example.course.exceptions.AlreadyExistException;
import com.example.course.services.json.impls.MovieJsonServiceImpl;
import com.example.course.services.json.impls.TicketJsonServiceImpl;
import com.example.course.services.json.impls.UserJsonServiceImpl;
import com.example.course.utility.FileUtility;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.time.LocalDateTime;
import java.util.*;

@Slf4j
public class TicketJsonServiceTest {

    private TicketJsonServiceImpl ticketJsonService;
    private UserJsonServiceImpl userJsonService;
    private MovieJsonServiceImpl movieJsonService;

    @SneakyThrows
    @BeforeTest
    public void setUp() {
        log.info("Entering BeforeTest");
        File tempFile = FileUtility.getTempFile();
        tempFile.deleteOnExit();
        userJsonService = new UserJsonServiceImpl(FileUtility.getUserFile());
        movieJsonService = new MovieJsonServiceImpl(FileUtility.getMovieFile());
        ticketJsonService = new TicketJsonServiceImpl(tempFile);

        ticketJsonService.create(Ticket.builder()
                .id(UUID.fromString("8dda7f4e-ec33-4073-927b-6991ef2069db"))
                .start(LocalDateTime.of(2021, 7, 8, 22, 0))
                .price(40.0f)
                .user(userJsonService.getAll().get(0))
                .movie(movieJsonService.getAll().get(0))
                .build());
        ticketJsonService.create(Ticket.builder()
                .id(UUID.fromString("f8301580-9cfa-4b88-9fa7-2062345663a6"))
                .start(LocalDateTime.of(2021, 7, 9, 16, 0))
                .price(40.0f)
                .user(userJsonService.getAll().get(0))
                .movie(movieJsonService.getAll().get(0))
                .build());
        ticketJsonService.create(Ticket.builder()
                .id(UUID.fromString("6c2b50c4-0de2-43fb-adac-7e557a86d141"))
                .start(LocalDateTime.of(2021, 7, 8, 22, 0))
                .price(10.0f)
                .user(userJsonService.getAll().get(1))
                .movie(movieJsonService.getAll().get(1))
                .build());
        List<Ticket> ticketList = ticketJsonService.getAll();
        Assert.assertEquals(ticketList.size(), 3);
    }

    @Test(groups = { "crud" })
    public void whenGetAll_thenReturnTicketList() {
        log.info("Entering Test");
        List<Ticket> foundList = ticketJsonService.getAll();
        Assert.assertEquals(foundList.size(), 3);
    }


    @Test(groups = { "crud" })
    public void whenGetById_thenReturnSingleTicket() {
        log.info("Entering Test");
        List<Ticket> actualList = ticketJsonService.getAll();
        Ticket expectedTicket = actualList.get(0);
        Ticket foundTicket = ticketJsonService.getById(expectedTicket.getId()).get();

        Assert.assertNotNull(foundTicket);
        Assert.assertSame(foundTicket, expectedTicket);
    }


    @SneakyThrows
    @Test(groups = { "crud" })
    public void whenCreateTicket_thenVoid() {
        log.info("Entering Test");
        Ticket ticket = Ticket.builder()
                .id(UUID.fromString("66c756c6-6aa5-475b-9d42-9825856e84db"))
                .user(userJsonService.getAll().get(0))
                .movie(movieJsonService.getAll().get(0))
                .start(LocalDateTime.of(2022, 1, 20, 15, 10))
                .price(10f)
                .build();
        ticketJsonService.create(ticket);

        List<Ticket> existedTickets = ticketJsonService.getAll();
        Assert.assertTrue(existedTickets.stream().anyMatch(existedTicket -> existedTicket.getId().equals(ticket.getId())));
    }


    @Test(groups = { "crud" })
    public void whenUpdateTicket_thenVoid() {
        log.info("Entering Test");
        List<Ticket> actualList = ticketJsonService.getAll();
        Ticket existedTicket = actualList.get(0).toBuilder().price(20f).build();

        ticketJsonService.update(existedTicket);
        Assert.assertEquals(Optional.of(ticketJsonService.getById(existedTicket.getId()).get().getPrice()), Optional.of(20f));
    }


    @Test(groups = { "crud" })
    public void whenDeleteTicket_thenVoid() {
        log.info("Entering Test");
        List<Ticket> actualList = ticketJsonService.getAll();
        Ticket ticketToDelete = actualList.get(0);

        ticketJsonService.delete(ticketToDelete.getId());
        Assert.assertTrue(ticketJsonService.getById(ticketToDelete.getId()).isEmpty());
    }


    @Test(groups = { "business" })
    public void whenGetMoviesForToday_thenReturnMovieList() {
        log.info("Entering Test");
        List<Movie> actualList = ticketJsonService.getMoviesForToday();
        Assert.assertEquals(actualList.size(), 1);
    }

    @Test(groups = { "business" })
    public void whenGetUsersForParticularSession_thenReturnUserList() {
        log.info("Entering Test");
        List<User> actualList = ticketJsonService.getUsersForParticularSession(
                movieJsonService.getAll().get(1),
                LocalDateTime.of(2021, 7, 8, 22, 0));
        Assert.assertEquals(actualList.size(), 1);
    }

    @Test(groups = { "business" })
    public void whenGetSumForMonth_thenReturnDouble() {
        log.info("Entering Test");
        double actualSum = ticketJsonService.getSumForMonth(2021, 7).getSum();
        Assert.assertEquals(actualSum, 50);
    }

    @Test(groups = { "business" })
    public void whenGetMovieRateForVisitingCount_thenReturnMovieList() {
        log.info("Entering Test");
        List<Movie> actualList = ticketJsonService.getMovieRateForVisitingCount();

        Assert.assertEquals(actualList.get(0), movieJsonService.getAll().get(1));
        Assert.assertEquals(actualList.get(1), movieJsonService.getAll().get(0));
    }

    @Test(groups = { "business" })
    public void whenGetMoviesWithMinThicketsSold_thenReturnMovieList() {
        log.info("Entering Test");
        List<Movie> actualList = ticketJsonService.getMoviesWithMinThicketsSold(2);
        Assert.assertEquals(actualList.get(0), movieJsonService.getAll().get(1));
    }
}
