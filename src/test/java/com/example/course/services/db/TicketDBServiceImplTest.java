package com.example.course.services.db;

import com.example.course.entities.Movie;
import com.example.course.entities.Ticket;
import com.example.course.entities.User;
import com.example.course.entities.enums.GenreTypes;
import com.example.course.services.connections.H2DataSource;
import com.example.course.services.db.impls.MovieDBServiceImpl;
import com.example.course.services.db.impls.TicketDBServiceImpl;
import com.example.course.services.db.impls.UserDBServiceImpl;
import com.example.course.utility.DBUtility;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class TicketDBServiceImplTest {

    private Connection connection;
    private IUserDBService userDBService;
    private IMovieDBService movieDBService;
    private ITicketDBService ticketDBService;

    @SneakyThrows
    @BeforeTest
    public void setUp() {
        log.info("Entering BeforeTest");
        connection = DBUtility.initConnectionWithTables(H2DataSource.getConnection());

        userDBService = new UserDBServiceImpl(connection);
        movieDBService = new MovieDBServiceImpl(connection);
        ticketDBService = new TicketDBServiceImpl(connection);

        userDBService.create(User.builder()
                .phone("+38 (000) 00 00 000")
                .name("First User")
                .build());
        userDBService.create(User.builder()
                .phone("+38 (000) 11 00 011")
                .name("First User")
                .build());
        userDBService.create(User.builder()
                .phone("+38 (000) 22 00 222")
                .name("First User")
                .build());

        List<User> userList = userDBService.getAll();
        Assert.assertEquals(userList.size(), 3);

        movieDBService.create(Movie.builder()
                .title("First Movie Title")
                .duration(163)
                .genres(Arrays.asList(
                        GenreTypes.ACTION,
                        GenreTypes.FANTASY
                ))
                .rating(7.9f)
                .build());
        movieDBService.create(Movie.builder()
                .title("Second Movie Title")
                .duration(142)
                .genres(Arrays.asList(
                        GenreTypes.DRAMA
                ))
                .rating(6.8f)
                .build());
        movieDBService.create(Movie.builder()
                .title("Third Movie Title")
                .duration(157)
                .genres(Arrays.asList(
                        GenreTypes.COMEDY
                ))
                .rating(5.1f)
                .build());
        List<Movie> movieList = movieDBService.getAll();
        Assert.assertEquals(movieList.size(), 3);

        ticketDBService.create(Ticket.builder()
                .start(LocalDateTime.of(2021, 7, 8, 22, 0))
                .price(35.0f)
                .user(userList.get(0))
                .movie(movieList.get(0))
                .build());
        ticketDBService.create(Ticket.builder()
                .start(LocalDateTime.of(2021, 7, 8, 21, 0))
                .price(40.0f)
                .user(userList.get(1))
                .movie(movieList.get(0))
                .build());
        ticketDBService.create(Ticket.builder()
                .start(LocalDateTime.of(2021, 7, 8, 20, 0))
                .price(40.0f)
                .user(userList.get(2))
                .movie(movieList.get(0))
                .build());

        ticketDBService.create(Ticket.builder()
                .start(LocalDateTime.of(2021, 8, 9, 12, 0))
                .price(40.0f)
                .user(userList.get(0))
                .movie(movieList.get(1))
                .build());
        ticketDBService.create(Ticket.builder()
                .start(LocalDateTime.of(2021, 8, 9, 12, 0))
                .price(40.0f)
                .user(userList.get(1))
                .movie(movieList.get(1))
                .build());

        ticketDBService.create(Ticket.builder()
                .start(LocalDateTime.of(2021, 8, 9, 19, 0))
                .price(40.0f)
                .user(userList.get(2))
                .movie(movieList.get(2))
                .build());

        List<Ticket> ticketList = ticketDBService.getAll();

        Assert.assertEquals(userList.size(), 3);
        Assert.assertEquals(movieList.size(), 3);
        Assert.assertEquals(ticketList.size(), 6);
    }

    @Test(groups = { "db-crud" })
    public void whenGetAll_thenReturnTicketList() {
        log.info("Entering Test");
        List<Ticket> foundList = ticketDBService.getAll();
        Assert.assertEquals(foundList.size(), 6);
    }

    @Test(groups = { "db-crud" })
    public void whenGetById_thenReturnSingleTicket() {
        log.info("Entering Test");
        List<Ticket> actualList = ticketDBService.getAll();
        Ticket expectedTicket = actualList.get(0);
        Ticket foundTicket = ticketDBService.getById(expectedTicket.getId()).get();

        Assert.assertNotNull(foundTicket);
        Assert.assertSame(foundTicket, expectedTicket);
    }

    @Test(groups = { "db-crud" })
    public void whenCreateTicket_thenVoid() {
        log.info("Entering Test");
        Ticket ticket = Ticket.builder()
                .start(LocalDateTime.of(2021, 8, 1, 10, 0))
                .price(35.0f)
                .user(userDBService.getAll().get(0))
                .movie(movieDBService.getAll().get(0))
                .build();
        ticketDBService.create(ticket);

        List<Ticket> existedTickets = ticketDBService.getAll();
        Assert.assertTrue(existedTickets.stream().anyMatch(existedTicket -> existedTicket.getPrice().equals(ticket.getPrice())));
    }

    @Test(groups = { "db-crud" })
    public void whenUpdateTicket_thenVoid() {
        log.info("Entering Test");
        List<Ticket> actualList = ticketDBService.getAll();
        Assert.assertEquals(actualList.size(), 6);

        Ticket existedTicket = actualList.get(0).toBuilder().price(100f).build();
        ticketDBService.update(existedTicket);
        Assert.assertEquals(java.util.Optional.of(ticketDBService.getById(existedTicket.getId()).get().getPrice()), java.util.Optional.of(100f));
    }

    @Test(groups = { "db-crud" })
    public void whenDeleteTicket_thenVoid() {
        log.info("Entering Test");
        List<Ticket> actualList = ticketDBService.getAll();
        Ticket ticketToDelete = actualList.get(6);

        ticketDBService.delete(ticketToDelete.getId());
        Assert.assertTrue(ticketDBService.getById(ticketToDelete.getId()).isEmpty());
    }


    @Test(groups = { "db-business" })
    public void whenGetMoviesForToday_thenReturnMovieList() {
        log.info("Entering Test");
        List<Movie> actualList = ticketDBService.getMoviesForToday();
        Assert.assertEquals(actualList.size(), 1);
    }

    @Test(groups = { "db-business" })
    public void whenGetUsersForParticularSession_thenReturnUserList() {
        log.info("Entering Test");
        List<User> actualList = ticketDBService.getUsersForParticularSession(
                movieDBService.getAll().get(2),
                LocalDateTime.of(2021, 8, 9, 19, 0));
        Assert.assertEquals(actualList.size(), 1);
    }

    @Test(groups = { "db-business" })
    public void whenGetSumForMonth_thenReturnFloat() {
        log.info("Entering Test");
        float actualSum = ticketDBService.getSumForMonth(2021, 7);
        Assert.assertEquals(actualSum, 115f);
    }

    @Test(groups = { "db-business" })
    public void whenGetMovieRateForVisitingCount_thenReturnMovieList() {
        log.info("Entering Test");
        List<Movie> actualList = ticketDBService.getMovieRateForVisitingCount();

        Assert.assertEquals(actualList.get(0), movieDBService.getAll().get(0));
        Assert.assertEquals(actualList.get(1), movieDBService.getAll().get(1));
        Assert.assertEquals(actualList.get(2), movieDBService.getAll().get(2));
    }

    @Test(groups = { "db-business" })
    public void whenGetMoviesWithMinThicketsSold_thenReturnMovieList() {
        log.info("Entering Test");
        List<Movie> actualList = ticketDBService.getMovieRateForVisitingCount();

        Assert.assertEquals(actualList.get(0), movieDBService.getAll().get(0));
        Assert.assertEquals(actualList.get(1), movieDBService.getAll().get(1));
        Assert.assertEquals(actualList.get(2), movieDBService.getAll().get(2));
    }
}
