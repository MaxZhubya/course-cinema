package com.example.course.services.db;

import com.example.course.entities.Movie;
import com.example.course.entities.Ticket;
import com.example.course.entities.User;
import com.example.course.entities.enums.GenreTypes;
import com.example.course.services.connections.H2DataSource;
import com.example.course.services.db.impls.MovieDBServiceImpl;
import com.example.course.services.db.impls.TicketDBServiceImpl;
import com.example.course.services.db.impls.UserDBServiceImpl;
import com.example.course.utility.DBUtility;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Slf4j
public class UserDBServiceImplTest {

    private Connection connection;
    private IUserDBService service;
    private IMovieDBService movieService;
    private ITicketDBService ticketService;

    @SneakyThrows
    @BeforeTest
    public void setUp() {
        log.info("Entering BeforeTest");
        connection = DBUtility.initConnectionWithTables(H2DataSource.getConnection());

        service = new UserDBServiceImpl(connection);
        movieService = new MovieDBServiceImpl(connection);
        ticketService = new TicketDBServiceImpl(connection);

        service.create(User.builder()
                .phone("+38 (000) 11 00 011")
                .name("First User")
                .build());

        service.create(User.builder()
                .phone("+38 (000) 22 00 022")
                .name("Second User")
                .build());
        List<User> userList = service.getAll();
        Assert.assertEquals(userList.size(), 2);

        movieService.create(Movie.builder()
                .title("First Movie Title")
                .duration(163)
                .genres(Arrays.asList(
                        GenreTypes.ACTION,
                        GenreTypes.FANTASY
                ))
                .rating(7.9f)
                .build());
        List<Movie> movieList = movieService.getAll();
        Assert.assertEquals(movieList.size(), 1);

        ticketService.create(Ticket.builder()
                .start(LocalDateTime.of(2021, 8, 1, 10, 0))
                .price(35.0f)
                .user(userList.get(0))
                .movie(movieList.get(0))
                .build());
        List<Ticket> ticketList = ticketService.getAll();

        Assert.assertEquals(userList.size(), 2);
        Assert.assertEquals(movieList.size(), 1);
        Assert.assertEquals(ticketList.size(), 1);
    }

    @Test(groups = { "db-crud" })
    public void whenGetAll_thenReturnUserList() {
        log.info("Entering Test");
        List<User> foundList = service.getAll();
        Assert.assertEquals(foundList.size(), 2);
    }

    @Test(groups = { "db-crud" })
    public void whenGetById_thenReturnSingleUser() {
        log.info("Entering Test");
        List<User> actualList = service.getAll();
        User expectedUser = actualList.get(0);
        User foundUser = service.getById(expectedUser.getId()).get();

        Assert.assertNotNull(foundUser);
        Assert.assertSame(foundUser, expectedUser);
    }

    @Test(groups = { "db-crud" })
    public void whenCreateUser_thenVoid() {
        log.info("Entering Test");
        User user = User.builder()
                .phone("+38 (222) 22 22 222")
                .name("Alexander Smirnov")
                .build();
        service.create(user);

        List<User> existedUsers = service.getAll();
        Assert.assertTrue(existedUsers.stream().anyMatch(existedUser -> existedUser.getPhone().equals(user.getPhone())));
    }

    @Test(groups = { "db-crud" })
    public void whenUpdateUser_thenVoid() {
        List<User> actualList = service.getAll();
        User existedUser = actualList.get(0).toBuilder().name("New First User").build();

        service.update(existedUser);
        Assert.assertEquals(service.getById(existedUser.getId()).get().getName(), "New First User");
    }

    @Test(groups = { "db-crud" })
    public void whenDeleteUser_thenVoid() {
        log.info("Entering Test");
        List<User> actualList = service.getAll();

        User userToDelete = actualList.get(0);
        Ticket ticketToDelete = ticketService.getAll().get(0);

        service.delete(userToDelete.getId());
        Assert.assertTrue(service.getById(userToDelete.getId()).isEmpty());
        Assert.assertTrue(ticketService.getById(ticketToDelete.getId()).isEmpty());
        Assert.assertEquals(movieService.getAll().size(), 1);
    }
}
