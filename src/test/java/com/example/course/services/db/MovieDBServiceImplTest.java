package com.example.course.services.db;

import com.example.course.entities.Movie;
import com.example.course.entities.Ticket;
import com.example.course.entities.User;
import com.example.course.entities.enums.GenreTypes;
import com.example.course.services.connections.H2DataSource;
import com.example.course.services.db.impls.MovieDBServiceImpl;
import com.example.course.services.db.impls.TicketDBServiceImpl;
import com.example.course.services.db.impls.UserDBServiceImpl;
import com.example.course.utility.DBUtility;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class MovieDBServiceImplTest {

    private Connection connection;
    private IUserDBService userDBService;
    private IMovieDBService movieDBService;
    private ITicketDBService ticketService;

    @SneakyThrows
    @BeforeTest
    public void setUp() {
        log.info("Entering BeforeTest");
        connection = DBUtility.initConnectionWithTables(H2DataSource.getConnection());

        userDBService = new UserDBServiceImpl(connection);
        movieDBService = new MovieDBServiceImpl(connection);
        ticketService = new TicketDBServiceImpl(connection);

        userDBService.create(User.builder()
                .phone("+38 (000) 11 00 011")
                .name("First User")
                .build());

        List<User> userList = userDBService.getAll();
        Assert.assertEquals(userList.size(), 1);

        movieDBService.create(Movie.builder()
                .title("First Movie Title")
                .duration(163)
                .genres(Arrays.asList(
                        GenreTypes.ACTION,
                        GenreTypes.FANTASY
                ))
                .rating(7.9f)
                .build());

        movieDBService.create(Movie.builder()
                .title("Second Movie Title")
                .duration(121)
                .genres(Arrays.asList(
                        GenreTypes.COMEDY
                ))
                .rating(5.8f)
                .build());
        List<Movie> movieList = movieDBService.getAll();
        Assert.assertEquals(movieList.size(), 2);

        ticketService.create(Ticket.builder()
                .start(LocalDateTime.of(2021, 8, 1, 10, 0))
                .price(35.0f)
                .user(userList.get(0))
                .movie(movieList.get(0))
                .build());
        List<Ticket> ticketList = ticketService.getAll();

        Assert.assertEquals(userList.size(), 1);
        Assert.assertEquals(movieList.size(), 2);
        Assert.assertEquals(ticketList.size(), 1);
    }

    @Test(groups = { "db-crud" })
    public void whenGetAll_thenReturnMovieList() {
        log.info("Entering Test");
        List<Movie> foundList = movieDBService.getAll();

        Assert.assertEquals(foundList.size(), 2);
    }

    @Test(groups = { "db-crud" })
    public void whenGetById_thenReturnSingleMovie() {
        log.info("Entering Test");
        List<Movie> actualList = movieDBService.getAll();
        Movie expectedMovie = actualList.get(0);
        Movie foundMovie = movieDBService.getById(expectedMovie.getId()).get();

        Assert.assertNotNull(foundMovie);
        Assert.assertSame(foundMovie, expectedMovie);
    }

    @Test(groups = { "db-crud" })
    public void whenCreateMovie_thenVoid() {
        log.info("Entering Test");
        Movie movie = Movie.builder()
                .title("Third Movie Title")
                .duration(68)
                .genres(Arrays.asList(
                        GenreTypes.FANTASY
                ))
                .rating(8.4f)
                .build();
        movieDBService.create(movie);

        List<Movie> existedMovies = movieDBService.getAll();
        Assert.assertTrue(existedMovies.stream().anyMatch(existedMovie -> existedMovie.getTitle().equals(movie.getTitle())));
    }

    @Test(groups = { "db-crud" })
    public void whenUpdateMovie_thenVoid() {
        log.info("Entering Test");
        List<Movie> actualList = movieDBService.getAll();
        Assert.assertEquals(actualList.size(), 2);

        Movie existedMovie = actualList.get(0).toBuilder().title("Changed Third Title").build();
        movieDBService.update(existedMovie);

        Assert.assertEquals(movieDBService.getById(existedMovie.getId()).get().getTitle(), "Changed Third Title");
    }

    @Test(groups = { "db-crud" })
    public void whenDeleteMovie_thenVoid() {
        log.info("Entering Test");
        List<Movie> actualList = movieDBService.getAll();

        Movie movieToDelete = actualList.get(0);

        Ticket ticketToDelete = ticketService.getAll().get(0);

        movieDBService.delete(movieToDelete.getId());
        Assert.assertTrue(movieDBService.getById(movieToDelete.getId()).isEmpty());
        Assert.assertTrue(ticketService.getById(ticketToDelete.getId()).isEmpty());
    }
}
