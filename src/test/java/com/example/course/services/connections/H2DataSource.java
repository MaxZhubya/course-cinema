package com.example.course.services.connections;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.dbcp.BasicDataSource;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

@NoArgsConstructor
public class H2DataSource {
    private static BasicDataSource ds = new BasicDataSource();

    private static Properties properties = new Properties();
    private static final String H2_PROPERTY_FILE = "C:\\Users\\tasvl\\IdeaProjects\\CinemaGradle\\src\\test\\resources\\h2.properties";

    static {
        FileReader reader = setReader();
        try {
            properties.load(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ds.setUrl(properties.getProperty("db.conn.url"));
        ds.setUsername(properties.getProperty("db.username"));
        ds.setPassword(properties.getProperty("db.password"));
        ds.setMinIdle(5);
        ds.setMaxIdle(10);
        ds.setMaxOpenPreparedStatements(100);
    }

    public static Connection getConnection() throws SQLException {
        Connection connection = ds.getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

    @SneakyThrows
    private static FileReader setReader() {
        return new FileReader(H2_PROPERTY_FILE);
    }
}
